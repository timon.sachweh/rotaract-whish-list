import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WhishCloudComponent } from './whish-cloud/whish-cloud.component';
import { WhishComponent } from './whish/whish.component';
import { WhishListComponent } from './whish-list/whish-list.component';
import { StartpageComponent } from './startpage/startpage.component';

@NgModule({
  declarations: [
    AppComponent,
    WhishCloudComponent,
    WhishComponent,
    WhishListComponent,
    StartpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
