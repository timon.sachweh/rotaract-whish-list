import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
// @ts-ignore
import whishes from '../../assets/whishes.json';

@Component({
  selector: 'app-whish-cloud',
  templateUrl: './whish-cloud.component.html',
  styleUrls: ['./whish-cloud.component.scss']
})
export class WhishCloudComponent implements OnInit, AfterViewInit {

  @ViewChild('wordcloud', {static: false}) wordcloud;

  generatedWishMap = [];

  constructor(private cdr: ChangeDetectorRef) { }


  ngAfterViewInit() {
    this.generateWishMap();
  }

  ngOnInit() {
  }

  private generateWishMap() {
    for (const w of whishes) {
      const newWish = {
        ...w,
        ...this.generateRandomPosition()
      };
      this.generatedWishMap.push(newWish);
    }
    console.log(this.generatedWishMap);
    this.cdr.detectChanges();
  }

  private generateRandomPosition() {
    const width = this.wordcloud.nativeElement.offsetWidth;
    const height = this.wordcloud.nativeElement.offsetHeight;

    return {
      x: Math.floor(Math.random() * width * 0.85),
      y: Math.floor(Math.random() * height * 0.9)
    };
  }
}
