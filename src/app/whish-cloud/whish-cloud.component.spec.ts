import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhishCloudComponent } from './whish-cloud.component';

describe('WhishCloudComponent', () => {
  let component: WhishCloudComponent;
  let fixture: ComponentFixture<WhishCloudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhishCloudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhishCloudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
