import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WhishCloudComponent } from './whish-cloud/whish-cloud.component';
import {WhishListComponent} from './whish-list/whish-list.component';
import {StartpageComponent} from './startpage/startpage.component';


const routes: Routes = [
  { path: 'whishes', component: WhishCloudComponent },
  { path: 'whisheslist', component: WhishListComponent },
  { path: 'start', component: StartpageComponent },
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: '**', redirectTo: '/whishes', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
