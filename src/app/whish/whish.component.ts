import {Component, ElementRef, HostListener, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-whish',
  templateUrl: './whish.component.html',
  styleUrls: ['./whish.component.scss']
})
export class WhishComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('x') x: string;
  // tslint:disable-next-line:no-input-rename
  @Input('y') y: string;
  // tslint:disable-next-line:no-input-rename
  @Input('whishTitle') title: string;
  // tslint:disable-next-line:no-input-rename
  @Input('name') name: string;
  // tslint:disable-next-line:no-input-rename
  @Input('buttonId') buttonId: string;

  colors = ['#F0A101', '#0061F5', '#B3E625', '#F01888', '#F06000',
    '#00E7F5', '#EBE200', '#F5A6E3', '#BEED6D', '#F2956F'];

  color = this.colors[1];

  moving = false;

  shift = {
    x: 0,
    y: 0
  };

  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    this.color = this.colors[Math.floor(Math.random() * this.colors.length)];
  }

  startMove(e) {
    const position = getPosition(e.currentTarget);
    this.shift = {
      x: e.pageX - position.left,
      y: e.pageY - position.top
    };
    this.moving = true;
  }

  @HostListener('document:mousemove', ['$event'])
  move(event: MouseEvent) {
    if (this.moving) {
      this.y = (event.clientY - this.shift.y) + 'px';
      this.x = (event.clientX - this.shift.x) + 'px';
    }
  }

  @HostListener('document:mouseup')
  stopMove() {
    this.moving = false;
  }

}


function getPosition(elem) {
  const box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };
}
