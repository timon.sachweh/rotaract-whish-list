import { Component, OnInit } from '@angular/core';
// @ts-ignore
import whishes from '../../assets/whishes.json';

@Component({
  selector: 'app-whish-list',
  templateUrl: './whish-list.component.html',
  styleUrls: ['./whish-list.component.scss']
})
export class WhishListComponent implements OnInit {
  public whishes = whishes;
  constructor() { }

  ngOnInit() {
  }

}
